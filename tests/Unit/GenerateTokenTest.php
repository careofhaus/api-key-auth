<?php

namespace HausTech\ApiKeyAuth\Tests\Unit;

use HausTech\ApiKeyAuth\Models\ApiKey;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Artisan;
use HausTech\ApiKeyAuth\Tests\TestCase;

class GenerateTokenTest extends TestCase
{
  use RefreshDatabase;

  /** @test */
  function the_command_generates_and_sets_and_api_token_with_required_arguments()
  {
      $application = 'unit_test_app';
      
      Artisan::call('generate:apikey ' . $application);

      $result = ApiKey::latest()->first();

      $this->assertCorrect($result, $application);
  }

  /** @test */
  function the_command_generates_and_sets_and_api_token_with_optional_arguments()
  {
    $application = 'another_unit_test_app';
    $ip = '1.2.3.4';

    Artisan::call('generate:apikey ' . $application . ' ' . $ip);

    $result = ApiKey::latest()->first();

    $this->assertCorrect($result, $application, $ip);
  }

  private function assertCorrect($result, $application, $ip = null)
  {
    $this->assertNotNull($result);
    $this->assertEquals($application, $result->first()->application);
    $this->assertIsString($result->first()->key);
    $this->assertEquals($ip, $result->first()->ip);
  }
}