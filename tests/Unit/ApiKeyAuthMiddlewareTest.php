<?php

namespace HausTech\ApiKeyAuth\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use HausTech\ApiKeyAuth\Http\Middleware\ApiKeyAuth;
use HausTech\ApiKeyAuth\Models\ApiKey;
use HausTech\ApiKeyAuth\Tests\TestCase;
use Illuminate\Support\Facades\Artisan;

use function PHPUnit\Framework\assertNull;

class ApiKeyAuthMiddlewareTest extends TestCase
{
  use RefreshDatabase;

  /** @test */
  function it_calls_next_if_api_key_is_valid()
  {
    Artisan::call('generate:apikey testapp');
    $key = ApiKey::all()->first()->key;

    // Given we have a request
    $request = new Request();
    $request->headers->set('token', $key);

    // when we pass the request to this middleware,
    // it should pass on the requet to the next() function
    // since the API key is valid.
    (new ApiKeyAuth())->handle($request, function ($reqFromNext) use ($request) {
      $this->assertEquals($request, $reqFromNext);
    });
  }

  /** @test */
  function it_returns_401_if_the_token_is_invalid()
  {
    $request = new Request();
    $request->headers->set('token', 'thisisaninvalidapikey');

    $this->assertUnauthorized($request);
  }

  /** @test */
  function it_returns_401_if_the_token_is_missing()
  {
    $request = new Request();

    $this->assertUnauthorized($request);
  }

  private function assertUnauthorized($request) 
  {
    // The closure function will never be called since unauthorized should return a 401 response
    $response = (new ApiKeyAuth())->handle($request, fn ($reqFromNext) => assertNull($reqFromNext));

    $this->assertEquals(401, $response->status());
    $this->assertFalse($response->isSuccessful());
  }
}