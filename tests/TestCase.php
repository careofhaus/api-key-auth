<?php

namespace HausTech\ApiKeyAuth\Tests;

use HausTech\ApiKeyAuth\ApiKeyAuthServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase
{
  public function setUp(): void
  {
    parent::setUp();
    // additional setup
  }

  protected function getPackageProviders($app)
  {
    return [
      ApiKeyAuthServiceProvider::class,
    ];
  }

  protected function getEnvironmentSetUp($app)
  {
    // import the CreateApiKeysTable class from the migration
    include_once __DIR__ . '/../database/migrations/2023_02_16_100000_create_api_keys_table.php';

    // run the up() method of that migration class
    // (new \CreateApiKeysTable)->up();  
  }
}