<?php

namespace HausTech\ApiKeyAuth;

use HausTech\ApiKeyAuth\Console\GenerateToken;
use HausTech\ApiKeyAuth\Http\Middleware\ApiKeyAuth;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class ApiKeyAuthServiceProvider extends ServiceProvider
{
  public function register()
  {
    //
  }

  public function boot()
  {
    $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

    if ($this->app->runningInConsole()) {
      $this->commands([
        GenerateToken::class,
      ]);
    }

    $router = $this->app->make(Router::class);
    $router->aliasMiddleware('api_key', ApiKeyAuth::class);
  }
}