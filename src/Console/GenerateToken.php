<?php

namespace HausTech\ApiKeyAuth\Console;

use HausTech\ApiKeyAuth\Models\ApiKey;
use Illuminate\Console\Command;

class GenerateToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:apikey {application} {ip?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate token for api';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // https://www.php.net/manual/en/function.random-bytes.php
        // Divide key length in two because each byte holds two hexadecimal characters.
        // E.g. 16 bytes => 32 character long token
        $token = bin2hex(random_bytes(env('API_KEY_LENGTH', 64) / 2));

        ApiKey::create([
            'key' => $token,
            'application' => $this->argument('application'),
            'ip' => $this->argument('ip'),
        ]);

        $this->info('API key: '.$token);

        return Command::SUCCESS;
    }
}
