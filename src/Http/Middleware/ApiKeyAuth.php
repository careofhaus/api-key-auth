<?php

namespace HausTech\ApiKeyAuth\Http\Middleware;

use Closure;
use HausTech\ApiKeyAuth\Models\ApiKey;
use Illuminate\Http\Request;

class ApiKeyAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function handle(Request $request, Closure $next)
    {
        try {
            $match = ApiKey::where('key', $request->header('TOKEN'))
                ->first();

            if (is_null($match) || ($match->ip && $match->ip !== $request->ip())) {
                return response()->json('Unauthorized', 401);
            }

            return $next($request);
        } catch (\Throwable $th) {
            return response()->json(null, 500);
        }
    }
}
